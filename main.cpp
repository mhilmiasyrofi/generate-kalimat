#include <bits/stdc++.h>

using namespace std;

struct point {
    int count;
    string prev;
    string next;
};

void makePoint(point &p) {
    p.count = 0;
}

// tipe bentukan untuk menyimpan 2char dan jumlahnya
struct datum {
    int count; // jumlah hitungan
    char chars[2]; // nilai 2 char
    vector<point> position; // digunakan untuk menyimpan posisi char 2
};

// mengecek apakah 2 char sudah ada di data
// jika ada maka mengembalikan index tempat ditemukan di data
int isExist(vector<datum> d, char c[2]) {

    int idx = -1;

    for (int i = 0; i < d.size(); i++) {
        if (!strcmp(d[i].chars,c)){
            idx = i; 
            break;
        } 
    }

    return idx;
}

//mengecek suatu karakter adalah termasuk dalam alfabet
bool isValid (char c) {
    return ( (c >=  'a' && c <= 'z') || (c >=  'A' && c <= 'Z') );
}

//membaca data dari file eksternal
void readRawData(string fileName, vector<string> &raw_data) {
    fileName.c_str();

    // Pembacaan file eksternal
    std::ifstream infile(fileName.c_str());
    std::string line;
    //baca per line
    while (std::getline(infile, line)){
        raw_data.push_back(line);
    }
}


// mencetak data ke terminal
void printData(vector<datum> data) {
    for (int i = 0; i < data.size(); i++) {
        cout << "char: " << data[i].chars << "\t jumlah: " << data[i].count << endl;
        for (int j = 0; j < data[i].position.size(); j++) {
            cout << "\t prev: " << data[i].position[j].prev.substr(0, 2) << endl;
            cout << "\t next: " << data[i].position[j].next.substr(0, 2) << endl;
            cout << "\t \t count: " << data[i].position[j].count << endl;
        }
    }
}

// mengurutkan data sesuai jumlah count
void sortData(vector<datum> &data) {
    for (int i = 0; i < data.size()-1; i++) {
        for (int j = i + 1; j < data.size(); j++) {
            if (data[i].count < data[j].count) {
                datum d = data[i];
                data[i] = data[j];
                data[j] = d;
            }
        }
    }
    for (int i = 0; i < data.size(); i++) {
        for (int m = 0; m < data[i].position.size() - 1; m++) {
            for (int n = m + 1; n < data[i].position.size(); n++) {
                if (data[i].position[m].count < data[i].position[n].count) {
                    point p = data[i].position[m];
                    data[i].position[m] = data[i].position[n];
                    data[i].position[n] = p;
                }
            }
        }
    }
}

// menyimpan data ke file eksternal
void saveData(vector<datum> data, string fileName) {
    ofstream file;
    file.open(fileName.c_str());
    // for (int i = 0; i < 10; i++) {
    for (int i = 0; i < data.size(); i++) {
        file << "char: " << data[i].chars << "\t jumlah: " << data[i].count << endl;
        for (int j = 0; j < data[i].position.size(); j++) {
            string p = data[i].position[j].prev.substr(0, 2);
            string n = data[i].position[j].next.substr(0, 2);
            // file << "\t prev: " << p << endl;
            // file << "\t next: " << n << endl;
            file << "\t prev: ";
            if ( p[1] == '\0') {
                file << "blank " << p[0] << endl;
            } else {
                file << p << endl;
            }

            file << "\t next: ";
            if (n[0] == '\0')
            {
                file << "blank blank" << endl;
            }
            else if (n[1] == '\0')
            {
                file << n[0] << " blank" << endl;
            }
            else
            {
                file << n << endl;
            }
            file << "\t \t count: " << data[i].position[j].count << endl;
        }
    }
    file.close();
}

string getPreviosString(string str, int idx) {
    string prev = "";
    string prevReverse = "";
    int i;
    if (idx == 0)
        return "";
    else
        i = idx-1;

    // terbalik
    while (isValid(str[i])) {
        prev.append(1, str[i]);
        i--;
    }

    for (int i = prev.size()-1 ; i >= 0 ; i--) {
        prevReverse.append(1, prev[i]);
    }

    return prevReverse; 
}

string getNextString(string str, int idx) {
    string next = "";
    int i;

    if (idx >= str.size()-1)
        return "";
    else
        i = idx + 2; // soalnya 2 char
        
    while (isValid(str[i])) {
        next.append(1, str[i]);
        i++;
    }

    return next;
    
}

int main() {

    // Data untuk menyimpan 2chars dan jumlahnya
    vector<datum> data;

    vector<string> raw_data;

    readRawData("corpus.txt", raw_data);

    string line;

    for (int i = 0; i < raw_data.size(); i++) {
        line = raw_data[i];
        for (int j = 0; j < line.size()-1; j++) {

            if (isValid(line[j]) && isValid(line[j+1])) {
                char c[2];
                c[0] = line[j];
                c[1] = line[j+1];
                c[2] = '\0';

                int val_idx = isExist(data, c);

                if ( val_idx == -1) {
                    datum instance;
                    instance.count = 1;
                    instance.chars[0] = c[0];
                    instance.chars[1] = c[1];
                    point p;
                    makePoint(p);
                    p.count = 1;
                    p.prev = getPreviosString(line, j);
                    p.next = getNextString(line, j);
                    instance.position.push_back(p);
                    data.push_back(instance);
                } else {
                    data[val_idx].count++;
                    point p;
                    makePoint(p);
                    p.count = 1;
                    p.prev = getPreviosString(line, j);
                    p.next = getNextString(line, j);
                    bool cek  = false;
                    int idx_pos;
                    for (int k =0; k < data[val_idx].position.size() && !cek; k++) {
                        if (p.prev.compare(data[val_idx].position[k].prev) == 0 && p.next.compare(data[val_idx].position[k].next) == 0) {
                            cek = true;
                            idx_pos = k;
                        }
                    }
                    if (cek) {
                        data[val_idx].position[idx_pos].count++;
                    } else {
                        data[val_idx].position.push_back(p);
                    }
                }
            }
        }
    }

    sortData(data);

    // printData(data);

    saveData(data, "output.txt");

    return 0;
}